import './App.css';
import React, {createContext, useContext, useEffect} from 'react';
import JoinGame from './components/JoinGame';
import JoinRoom from './components/JoinRoom';
import Room from './components/Room';
import Settings from './components/Settings';
import Topic from './components/Topic';
import Wait from './components/Wait';
import Question from './components/Question';
import Points from './components/Points';
import Results from './components/Results';
import {useSelector} from "react-redux";


function App(props) {

    const renderSwitch = (param) => {
        switch (param) {
            case 'JoinGame':
                return <JoinGame/>;
            case 'JoinRoom':
                return <JoinRoom/>;
            case 'Room':
                return <Room/>;
            case "Question":
                return <Question/>;
            case "Topic":
                return <Topic/>;
            case "Points":
                return <Points/>;
            case "Results":
                return <Results/>;
            case "Wait":
                return <Wait/>;
            case "Settings":
                return <Settings />;
            default:
                return <JoinGame/>;
        }
    }
    const componentType = useSelector((state) => state.gameState.componentType)
    useEffect(()=>{console.log(componentType)},[componentType])

    return (
        <div className="app-background">
            <div className="App">
                {renderSwitch(componentType)
                }
            </div>
        </div>
    );
}

export default App;

/*
<Router>
                <div>
                    <Routes>
                        <Route path="/" element={<JoinGame context={context}/>}/>
                        <Route path="/game" element={<JoinRoom context={context} contextSetter={contextSetter}/>}/>
                        <Route path="/room" element={<Room context={context} contextSetter={contextSetter}/>}/>
                        <Route path="/settings" element={<Settings context={context}/>}/>
                        <Route path="/topic" element={<Topic context={context}/>}/>
                        <Route path="/wait" element={<Wait context={context}/>}/>
                        <Route path="/question" element={<Question context={context}/>}/>
                        <Route path="/points" element={<Points context={context}/>}/>
                        <Route path="/results" element={<Results context={context}/>}/>
                    </Routes>
                </div>
            </Router>

 */
