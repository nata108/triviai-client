import Button from './util/Button'
import Text from './util/Text'
import Header from './util/Header';
import {addAction} from "./ActionQueue";
import {useDispatch, useSelector} from "react-redux";

const Results = (props) => {
    const gameInfoObj = useSelector(state => state.gameState.gameInfoObj)
    const roomInfoObj = useSelector(state => state.gameState.roomInfoObj)
    const leaderBoard = gameInfoObj.leaderBoard;
    const myName = useSelector(state => state.gameState.name)
    const dispatch = useDispatch();

    function medal(index) {
        if (index === 0) return '🥇 ';
        if (index === 1) return '🥈 ';
        if (index === 2) return '🥉 ';
        return '';
    }

    function adminButtons() {
        if (roomInfoObj.adminName === myName) {
            return (
                <div>
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "createRoom"}))
                        }}
                        data='Restart with settings'
                    />
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "leaveRoom"}))
                        }}
                        data='Leave room'
                    />
                </div>
            )
        } else {
            let message = 'Waiting for ' + roomInfoObj.adminName + ' to restart';
            return (
                <div>
                    {gameInfoObj.rejoinable ? <Button
                        onClick={() => {
                            dispatch(addAction({
                                type: "joinRoom",
                                roomCode: gameInfoObj.roomCode
                            }))
                        }}
                        data='Rejoin room'
                    /> :
                        <Text data={message}/>}
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "leaveRoom"}))
                        }}
                        data='Leave room'
                    />
                </div>
            )
        }
    }

    return (
        <div>
            <div style={containerStyle}>
                <div>
                    <Header/>
                </div>
                <div style={pointsStyle}>
                    <Text bold='true' data='Results'/>
                    {leaderBoard.map((player, index) => {
                        let display = player.name;
                        if (myName === player.name) display = display + ' (You)';
                        return (
                            <Text
                                key={index}
                                small='true'
                                data={medal(index) + display + ', ' + player.score}
                            />
                        )
                    })}
                </div>
                <div>
                    {adminButtons()}
                </div>
            </div>
        </div>
    )
}

export default Results;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '350px'
};

const pointsStyle = {
    height: '100%'
}
