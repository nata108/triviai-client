import Logo from './util/Logo'
import Button from './util/Button'
import Text from './util/Text'
import X from './util/X'
import Range from './util/Range'
import Toggle from './util/Toggle'
import RoomCode from "./util/RoomCode";
import { useDispatch, useSelector } from "react-redux";
import { addAction } from "./ActionQueue";
import {closeSettings} from "./GameStateSlice";
import { useState } from "react";

const Settings = (props) => {
    const dispatch = useDispatch();
    const roomInfoObj = useSelector(state => state.gameState.roomInfoObj)

    const [regularRounds, setRegularRounds] = useState(roomInfoObj.currentSettings.regularRounds);
    const [wildcardRounds, setWildcardRounds] = useState(roomInfoObj.currentSettings.wildcardRounds);
    const [roundLength, setRoundLength] = useState(roomInfoObj.currentSettings.roundLength);
    const [wildcardScoreMultiplier, setWildcardScoreMultiplier] = useState(roomInfoObj.currentSettings.wildcardScoreMultiplier);
    const [relativeScoring, setRelativeScoring] = useState(roomInfoObj.currentSettings.relativeScoring);

    let settings = {
        regularRounds: regularRounds,
        wildcardRounds: wildcardRounds,
        roundLength: roundLength,
        wildcardScoreMultiplier: wildcardScoreMultiplier,
        relativeScoring: relativeScoring
    }

    return (
        <div style={{position: "relative"}}>
            <X/>
            <div style={containerStyle}>
                <div>
                    <Logo small='true'/>
                    <RoomCode roomCode={roomInfoObj.roomCode}/>
                </div>
                <div style={playersStyle}>
                    <Text tiny='true' data={'Regular rounds: ' + regularRounds}/>
                    <Range input={setRegularRounds} min='3' max='15' default={regularRounds}/>
                    <Text tiny='true' data={'Wildcard rounds: ' + wildcardRounds}/>
                    <Range input={setWildcardRounds}  min='0' max='4' default={wildcardRounds}/>
                    <Text tiny='true' data={'Answer time: ' + roundLength + 's'}/>
                    <Range input={setRoundLength}  min='10' max='30' default={roundLength}/>
                    <Text tiny='true' data={'Wildcard multiplier: ' + wildcardScoreMultiplier + 'x'}/>
                    <Range disabled={wildcardRounds === 0} divide='2' input={setWildcardScoreMultiplier}  min='1' max='4' default={wildcardScoreMultiplier}/>
                    <Text tiny='true' data={'Relative points: ' + (relativeScoring ? 'Yes' : 'No')}/>
                    <Toggle input={setRelativeScoring} default={relativeScoring ? 1 : 0}/>
                </div>
                <div>
                    <Button onClick={() => {dispatch(addAction({type: "updateSettings", settings: settings}))}}
                            data='Save settings'/>
                    <Button onClick={() => {dispatch(closeSettings())}}
                            data='Back to room'/>
                </div>
            </div>
        </div>
    )
}

export default Settings;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '350px'
};

const playersStyle = {
    maxHeight: '400px',
    height: '100%'
}
