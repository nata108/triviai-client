import {createSlice} from '@reduxjs/toolkit'

export const actionSlice = createSlice({
    name: 'actionQueue',
    initialState: {
        queue: [],
    },
    reducers: {
        addAction: (state, action) => {
            console.log(action.payload)
            state.queue.push(action.payload)
        },
        consumeAction: (state) => {
            state.queue.pop()
        }
    },
})

// Action creators are generated for each case reducer function
export const {addAction, consumeAction} = actionSlice.actions

export default actionSlice.reducer
