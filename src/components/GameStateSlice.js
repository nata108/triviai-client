import {createSlice} from '@reduxjs/toolkit'

const GAMESTATE = {
    GAME_START: 0,
    TOPIC: 1,
    WAIT: 2,
    ROUND_START: 3,
    QUESTION: 4,
    GAME_END: 5
}

const ROUNDTYPE = {
    REGULAR: 0,
    WILDCARD: 1
}

export const gameStateSlice = createSlice({
    name: 'gameState',
    initialState: {
        connected: false,
        componentType: "JoinGame",
        name: "",
        roomCode: "",
        roomInfoObj: null,
        message: "",
        gameInfoObj: null
    },
    reducers: {
        processRegisterClientResponse: (state, action) => {
            console.log(action.payload.stateObj)
            state.message = action.payload.stateObj.message
            state.roomInfoObj = null;
            state.gameInfoObj = null;
            state.roomCode = "";
            if (action.payload.stateObj.state) {
                if(action.payload.name) {
                    state.name = action.payload.name;
                }
                state.componentType = "JoinRoom";
            } else {
                state.componentType = "JoinGame"
                alert(state.message)
            }
        },
        processRoomInfoUpdate: (state, action) => {
            state.message = action.payload.stateObj.message
            state.componentType = "Room"
            if (action.payload.stateObj.state) {
                state.roomCode = action.payload.roomInfoObj.roomCode
                state.roomInfoObj = action.payload.roomInfoObj
            }else{
                if(state.roomInfoObj) {
                    state.componentType = "Room"
                }
                else{
                    state.componentType = "JoinRoom"
                }
                alert(state.message)
            }
        },
        processGameInfoUpdate: (state, action) => {
            if (action.payload.stateObj.state) {
                let g = action.payload.gameInfoObj;
                state.gameInfoObj = g;
                state.roomCode = g.roomCode;
                console.log(g);
                switch (g.gameState) {
                    case GAMESTATE.GAME_START:
                        state.componentType = "Wait";
                        state.message = g.message;
                        break;
                    case GAMESTATE.WAIT:
                        state.componentType = "Wait";
                        if(g.questionObj){
                            if(g.questionObj.correctId !== null){
                                state.componentType = "Points";
                            }
                        }
                        state.message = g.message;
                        break;
                    case GAMESTATE.TOPIC:
                        state.componentType = "Topic";
                        state.message = g.message;
                        break;
                    case GAMESTATE.QUESTION:
                        if(g.questionObj) {
                            state.componentType = "Question";
                            state.message = g.message;
                        }
                        else{
                            state.componentType = "JoinGame";
                        }
                        break;
                    case GAMESTATE.ROUND_START:
                        state.componentType = "Wait";
                        if(g.questionObj){
                            if(g.questionObj.correctId !== null){
                                state.componentType = "Points";
                            }
                        }
                        state.message = g.message;
                        break;
                    case GAMESTATE.GAME_END:
                        state.componentType = "Results";
                        state.message = g.message;
                        break;
                    default:
                        break;
                }
            } else {
                state.message = action.payload.stateObj.message
                state.gameInfoObj = null;
                alert(state.message)
            }

        },

        connected: (state) => {
            state.connected = true
        },
        disconnected: (state) => {
            state.connected = false
        },
        openSettings: (state)=>{
           state.componentType="Settings"
        },
        closeSettings: (state)=>{
            state.componentType="Room"
        },
    },
})

// Action creators are generated for each case reducer function
export const {
    processRegisterClientResponse,
    processRoomInfoUpdate,
    connected,
    disconnected,
    processGameInfoUpdate,
    openSettings,
    closeSettings
} = gameStateSlice.actions

export default gameStateSlice.reducer
