import Text from './util/Text'
import Header from './util/Header';
import { useSelector } from "react-redux";

const Points = () => {
    const gameInfoObj = useSelector(state => state.gameState.gameInfoObj)
    const leaderBoard = gameInfoObj.leaderBoard;
    const myName = useSelector(state => state.gameState.name)

    const question = gameInfoObj?.questionObj?.question;
    const correctAnswer = gameInfoObj?.questionObj?.answers[gameInfoObj.questionObj.correctId];

    return (
        <div>
            <div style={containerStyle}>
                <div>
                    <Header/>
                </div>

                <div style={pointsStyle}>
                    <Text bold='true' data='Points'/>
                    {leaderBoard.map((player, index) => {
                        let display = player.name;
                        if (myName === player.name) display = display + ' (You)';
                        return (
                            <Text key={index} small='true' data={display + ', ' + player.score}/>
                        )
                    })}
                </div>
                <div>
                    <Text small='true' data={question}/>
                    <Text green='true' data={correctAnswer?.toUpperCase()}/>
                </div>
                <div style={{padding: '40px 0 0 0'}}>
                    <Text tiny='true' data='Get ready for the next one...'/>
                </div>
            </div>
        </div>
    )
}

export default Points;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '350px'
};

const pointsStyle = {
    height: '100%'
}
