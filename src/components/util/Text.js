const Text = (props) => {
    return (
        <div style={
            props.tiny
            ? tinyStyle
            : (props.small
                ? smallStyle
                : (props.bold
                    ? boldStyle
                    : (props.red
                        ? redStyle
                        : (props.green
                                ? greenStyle
                                : textStyle))))}>
            {props.data}
        </div>
    )
}

export default Text;

const greenStyle = {
    margin: '3% 0 0 0',
    color: '#cdcd30',
    fontSize: '32pt'
}

const redStyle = {
    margin: '3% 0 0 0',
    color: 'red',
    fontSize: '26pt'
}

const textStyle = {
    margin: '3% 0 0 0',
    color: "#F1F1F1",
    fontSize: '26pt'
}

const tinyStyle = {
    margin: '3% 0 0 0',
    color: "#F1F1F1",
    fontSize: '16pt'
}

const smallStyle = {
    margin: '3% 0 0 0',
    color: "#F1F1F1",
    fontSize: '22pt'
}

const boldStyle = {
    fontWeight: 'bold',
    margin: '3% 0 0 0',
    color: "#F1F1F1",
    fontSize: '20pt'
}
