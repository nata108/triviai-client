import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../Reducer'
import actionReducer from "../ActionQueue";
import gameStateReducer from "../GameStateSlice"

export default configureStore({
    reducer: {
        counter: counterReducer,
        actionQueue: actionReducer,
        gameState: gameStateReducer
    },
})
