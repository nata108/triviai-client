const Logo = (props) => {
    return (
        <div style={props.small ? smallLogoStyle : logoStyle}>
            TriviAI</div>
    )
}

export default Logo;

const logoStyle = {
    width: '100%',
    color: "#F1F1F1",
    fontSize: '72pt',
    textAlign:'center',
    margin: '0 0 30% 0',
    textShadow: '5px 5px #1A1A2D',
}

const smallLogoStyle = {
    width: '100%',
    color: "#F1F1F1",
    textAlign:'center',
    fontSize: '42pt',
    margin: '0 0 -15px 0',
    textShadow: '4px 4px #1A1A2D',
}
