import {useDispatch} from "react-redux";
import {addAction} from "../ActionQueue";

const X = () => {
const dispatch = useDispatch();
    return (
        <div>
            <div onClick={() => {dispatch(addAction({type: "leaveRoom"}))}}
                    style={closeStyle}>
                X
            </div>
        </div>
    )
}

export default X;

const closeStyle = {
    cursor: 'pointer',
    color: '#F1F1F1',
    fontWeight: 'bold',
    fontFamily: 'Patrick Hand',
    fontSize: '24pt',
    position: 'absolute',
    top: '-5px',
    right: '-20px',
    width: '45px',
    height: '45px',
    backgroundColor: '#1A1A2D'
};
