import Text from './Text'

const Range = (props) => {
    return (
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
            <Text tiny='true' data={props.min / (props.divide ?? 1)}/>
            <input disabled={props.disabled}
                   onChange={e => props.input(e.target.value / (props.divide ?? 1))}
                   style={rangeStyle} type="range"
                   min={props.min} max={props.max}
                   defaultValue={props.default * (props.divide ?? 1)}/>
            <Text tiny='true' data={props.max / (props.divide ?? 1)}/>
        </div>
    )
}

export default Range;

const rangeStyle = {
    width: "80%"
};
