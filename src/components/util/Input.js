const Input = (props) => {

    const toInputUppercase = e => {
        e.target.value = ('' + e.target.value).toUpperCase();
    };

    const noStyle = e => {
    };

    return (
        <div>
            <input onChange={e => props.input(e.target.value)}
                   style={inputStyle}
                   maxLength={props.roomCode === undefined ? 20 : 5}
                   onInput={props.roomCode === undefined ? noStyle : toInputUppercase}
                   placeholder={props.placeholder.toUpperCase()}/>
        </div>
    )
}

export default Input;

const inputStyle = {
    width: "70%",
    color: '#1A1A2D',
    opacity: '90%',
    backgroundColor: '#F1F1F1',
    padding: '3% 5% 3% 5%',
    margin: '3% 0 3% 0',
    textAlign: 'center',
    fontFamily: 'Patrick Hand',
    fontSize: '24pt'
};
