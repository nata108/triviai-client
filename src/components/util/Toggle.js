import Text from "./Text";

const Toggle = (props) => {
    return (
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
            <Text tiny='true' data='No'/>
            <input onChange={e => props.input(e.target.value === '1')}
                   style={toggleStyle} type="range"
                   min='0' max='1'
                   defaultValue={props.default}/>
            <Text tiny='true' data='Yes'/>
        </div>
    )
}

/* CHECKBOX HELYETT RANGE LESZ ITT IS

return (
        <div>
            <input onClick={e => {
                console.log(props.input);
                props.input(true);
            }}
                   style={checkboxStyle} type='checkbox'
                   checked={props.default}/>
        </div>
    )
 */

export default Toggle;

const checkboxStyle = {
    width: '30px',
    height: '30px'
};

const toggleStyle = {
    width: "80%"
};