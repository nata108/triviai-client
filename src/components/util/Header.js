import Logo from "./Logo";
import RoomCode from "./RoomCode";
import { useSelector } from "react-redux";

const Header = () => {
    const roomInfoObj = useSelector(state => state.gameState.roomInfoObj)

    return (
        <div style={{width: '350px', display: 'flex', justifyContent: 'space-between'}}>
            <Logo small='true'/>
            <RoomCode roomCode={roomInfoObj.roomCode}/>
        </div>
    )
}

export default Header;
