import Text from './Text'
import {useEffect, useRef, useState} from 'react'
import { useNavigate } from 'react-router-dom'
import {useDispatch} from "react-redux";
import {addAction} from "../ActionQueue";

function Clock(props) {
    const dispatch = useDispatch()
    const [counter, setCounter] = useState(props.start ?? 30);

    const dispatchCalled = useRef(false);
    // Third Attempts
    useEffect(() => {
        if(counter>0){
            var timeout = setTimeout(() => setCounter(counter - 1), 1000);
        }
        else if(!dispatchCalled.current){
            dispatch(addAction({type:"gameInfoUpdateRequest"}))
            dispatchCalled.current = true;
        }
        return () => clearTimeout(timeout);
    } );

    if (counter > 5) {
        return (
            <div>
                <Text data={counter}/>
            </div>
        )
    } else if (counter > 0) {
        return (
            <div>
                <Text red='true' data={counter}/>
            </div>
        )
    } else {
        return (
            <div>
                <Text red='true' data='0'/>
            </div>
        )
    }
}

export default Clock;
