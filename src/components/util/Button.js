import { useNavigate } from 'react-router-dom'

const Button = (props) => {
    //const navigate = useNavigate();

    return (
        <div>
            <button onClick={props.onClick} style={props.small ? smallButtonStyle : buttonStyle}
            >{props.data.toUpperCase()}</button>
        </div>
    )
}

export default Button;

const buttonStyle = {
    width: '100%',
    color: "#1A1A2D",
    opacity: '70%',
    backgroundColor: "#F1F1F1",
    padding: '5% 5% 5% 5%',
    margin: '3% 0',
    fontFamily: 'Patrick Hand',
    fontSize: '22pt',
    cursor: 'pointer'
};

const smallButtonStyle = {
    width: "100%",
    color: "#1A1A2D",
    opacity: '70%',
    backgroundColor: "#F1F1F1",
    padding: '2%',
    margin: '3% 0',
    fontFamily: 'Patrick Hand',
    fontSize: '16pt',
    cursor: 'pointer'
};
