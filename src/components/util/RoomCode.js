const RoomCode = (props) => {
    return (
        <div style={roomCodeStyle}>
            Room code: {props.roomCode}
        </div>
    )
}

export default RoomCode;

const roomCodeStyle = {
    color: "#F1F1F1",
    fontSize: '18pt',
    textShadow: '5px 5px #1A1A2D',
    width: "100%"
}
