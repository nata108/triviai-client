import Logo from './util/Logo'
import Text from './util/Text'
import Input from './util/Input'
import Button from './util/Button'
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addAction } from "./ActionQueue";

const JoinRoom = (props) => {
    const [roomCode, setRoomCode] = useState();
    const dispatch = useDispatch();

    return (
        <div>
            <div>
                <Logo/>
            </div>
            <div>
                <Button onClick={() => {dispatch(addAction({type: "createRoom"}))}}
                        data='Create room'/>
            </div>
            <div>
                <Text data='or'/>
            </div>
            <div>
                <Input input={setRoomCode} placeholder='Room code' roomCode='true'/>
                <Button onClick={() => {dispatch(addAction({type: "joinRoom", roomCode: roomCode}))}}
                         data='Join room'/>
            </div>
        </div>
    )
}

export default JoinRoom;
