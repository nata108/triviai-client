import Logo from './util/Logo'
import Text from './util/Text'
import Input from './util/Input'
import Button from './util/Button'
import { useDispatch, useSelector } from "react-redux";
import { addAction } from "./ActionQueue";
import { useState } from "react";

const JoinGame = (props) => {
    const [nickname, setNickname] = useState('');
    const dispatch = useDispatch();

    return (
        <div>
            <div>
                <Logo/>
            </div>
            <div>
                <Text data={'Enter your name'}/>
                <Input input={setNickname} placeholder='Nickname'/>
                <Button onClick={() => {dispatch(addAction({type: "registerClient", name: nickname}))}}
                        data='Join game'/>
            </div>
        </div>
    )
}

export default JoinGame;
