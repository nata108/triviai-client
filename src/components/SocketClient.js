import {Component, useEffect, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {increment} from "./Reducer";
import {consumeAction} from "./ActionQueue";
import {
    connected, processGameInfoUpdate,
    processRegisterClientResponse,
    processRoomInfoUpdate
} from "./GameStateSlice";

class SocketClient {
    constructor(url, callback) {
        const ws = new WebSocket(url);

        this.send = function (message, callback) {
            this.waitForConnection(function () {
                ws.send(JSON.stringify(message));
                if (typeof callback !== 'undefined') {
                    callback();
                }
            }, 100);
        };

        this.waitForConnection = function (callback, interval) {
            if (ws.readyState === 1) {
                callback();
            } else {
                var that = this;
                // optional: implement backoff for interval here
                setTimeout(function () {
                    that.waitForConnection(callback, interval);
                }, interval);
            }
        };

        ws.onopen = () => {
            console.log("Connecting")
        }
        ws.onclose = () => {
            console.log("Disconnected")
        }
        ws.onmessage = (data) => {
            callback(data)
        }
    }

    sendRegisterClient(name) {
        this.send({type: "RegisterClient", name: name})
    }

    sendCreateRoom() {
        this.send({type: "CreateRoom"})
    }

    sendJoinRoom(roomCode) {
        this.send({type: "JoinRoom", roomCode: roomCode})
    }

    sendUpdateRoomSettings(
        settings
    ) {
        this.send({
            type: "UpdateRoomSettings",
            settingsObj: settings
        })
    }

    sendLeaveRoom() {
        this.send({type: "LeaveRoom"})
    }

    sendStartGame() {
        this.send({type: "StartGame"})
    }

    sendTopic(topic1, topic2) {
        this.send({type: "SendTopic", topics: [topic1, topic2]})
    }

    sendAnswer(answerId) {
        this.send({type: "SendAnswer", answerId: answerId})
    }

    sendGameInfoUpdateRequest() {
        this.send({type: "GameInfoUpdateRequest"})
    }

}

const SocketWrapper = (props) => {
    const ws = useRef(null);
    console.log("rerender")
    var actionQueue = useSelector((state) => state.actionQueue.queue)

    useEffect(() => {
        if (actionQueue != null && ws.current != null) {
            console.log(actionQueue.length)
            if (actionQueue.length >= 1) {
                switch (actionQueue[0].type) {
                    case "registerClient":
                        ws.current.sendRegisterClient(actionQueue[0].name);
                        break;
                    case "createRoom":
                        ws.current.sendCreateRoom();
                        break;
                    case "joinRoom":
                        ws.current.sendJoinRoom(actionQueue[0].roomCode)
                        break;
                    case "updateSettings":
                        ws.current.sendUpdateRoomSettings(actionQueue[0].settings)
                        break;
                    case "leaveRoom":
                        ws.current.sendLeaveRoom();
                        break;
                    case "startGame":
                        ws.current.sendStartGame();
                        break;
                    case "topic":
                        ws.current.sendTopic(actionQueue[0].topics[0],actionQueue[0].topics[1])
                        break;
                    case "answer":
                        ws.current.sendAnswer(actionQueue[0].answerId);
                        break;
                    case "gameInfoUpdateRequest":
                        ws.current.sendGameInfoUpdateRequest();
                        break;
                    default:
                        break;
                }
                dispatch(consumeAction());
            }
        };
    }, [actionQueue, props]);

    const dispatch = useDispatch();

    useEffect(() => {
        console.log("Started")
        ws.current = new SocketClient(props.url, (msg) => {
            console.log(msg.data.toString())
            if(msg.data){
                const data = JSON.parse(msg.data);
                switch(data.type){
                    case "RegisterClientResponse":
                        dispatch(processRegisterClientResponse(data));
                        break;
                    case "RoomInfoUpdate":
                        dispatch(processRoomInfoUpdate(data));
                        break;
                    case "GameInfoUpdate":
                        dispatch(processGameInfoUpdate(data));
                        break;
                    default:
                        break;
                }
            }
        })
        dispatch(connected())
        return () => null;
    }, []);


    return (false);
}

export default SocketWrapper;
