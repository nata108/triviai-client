import Button from './util/Button'
import Text from './util/Text'
import Header from './util/Header';
import Clock from "./util/Clock";
import {useDispatch, useSelector} from "react-redux";
import {addAction} from "./ActionQueue";
import wildcardImg from '../assets/wildcard.png';

const Question = (props) => {
    const gameInfoObj = useSelector(state => state.gameState.gameInfoObj)
    const dispatch = useDispatch();
    const questionObj = gameInfoObj.questionObj;

    return (
        <div>
            <div style={containerStyle}>

                <div>
                    <Header/>
                </div>
                <div style={questionStyle}>
                    <div
                        style={{
                            width: "100%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        {gameInfoObj.roundType ? <img
                            src={wildcardImg}
                            style={{width: "10%", marginRight: "3%"}}
                        /> : null}<Text
                        bold='true'
                        data={gameInfoObj.message}
                    />{gameInfoObj.roundType ? <img
                        src={wildcardImg}
                        style={{width: "10%", marginLeft: "3%"}}
                    /> : null}
                    </div>

                    <Text data={questionObj.question}/>
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "answer", answerId: 0}))
                        }}
                        small='true' data={questionObj.answers[0]}
                    />
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "answer", answerId: 1}))
                        }}
                        small='true' data={questionObj.answers[1]}
                    />
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "answer", answerId: 2}))
                        }}
                        small='true' data={questionObj.answers[2]}
                    />
                    <Button
                        onClick={() => {
                            dispatch(addAction({type: "answer", answerId: 3}))
                        }}
                        small='true' data={questionObj.answers[3]}
                    />
                </div>
                <div>
                    <Clock start={questionObj.remainingTime}/>
                </div>
            </div>
        </div>
    )
}

export default Question;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '90%',
    marginLeft: '5%'
};

const questionStyle = {
    height: '100%'
}
