import Logo from './util/Logo'
import Button from './util/Button'
import Text from './util/Text'
import X from './util/X'
import RoomCode from "./util/RoomCode";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addAction } from "./ActionQueue";
import {openSettings} from "./GameStateSlice";

const Room = (props) => {
    const dispatch = useDispatch();
    const [players, setPlayers] = useState(['Rezsek']);
    const roomInfoObj = useSelector(state => state.gameState.roomInfoObj)
    const myName = useSelector(state => state.gameState.name)

    let playersHeader = 'Players (' + roomInfoObj.userNames.length + '/8):';

    useEffect(() => {
        setPlayers(roomInfoObj.userNames ?? ['Rezsek', 'Stux', 'So']);
        playersHeader = 'Players (' + roomInfoObj.userNames.length + '/8):';
    }, [roomInfoObj.userNames])

    function adminButtons() {
        if (roomInfoObj.adminName === myName) {
            return (
                <div>
                    <Button onClick={() => {dispatch(openSettings())}}
                            data='Game settings'/>
                    <Button onClick={() => {dispatch(addAction({type: "startGame"}))}}
                            data='Start game'/>
                </div>
            )
        } else {
            let message = 'Waiting for ' + roomInfoObj.adminName + ' to start';
            return (
                <div>
                    <Text tiny='true' data={message} />
                </div>
            )
        }
    }

    return (
        <div style={{position: "relative"}}>
            <X/>
            <div style={containerStyle}>
                <div>
                    <Logo small='true'/>
                    <RoomCode roomCode={roomInfoObj.roomCode}/>
                </div>
                <div style={playersStyle}>
                    <Text bold='true' data={playersHeader}/>
                    {players.map((player, index) => {
                        let display = player;
                        if (roomInfoObj.adminName === player) display = '👑 ' + player;
                        if (myName === player) display = display + ' (You)';
                        return (
                            <Text key={index} small='true' data={display}/>
                        )
                    })}
                </div>
                <div>
                    {adminButtons()}
                </div>
            </div>
        </div>
    )
}

export default Room;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '350px'
};

const playersStyle = {
    maxHeight: '400px',
    height: '100%'
}
