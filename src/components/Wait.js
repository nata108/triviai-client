import Text from './util/Text'
import Loading from './../assets/ajax-loader.gif'
import Header from './util/Header';
import {useSelector} from "react-redux";

const Wait = () => {
    const waitMessage = useSelector(state => state.gameState.message)
    return (
        <div>
            <div style={containerStyle}>
                <div>
                    <Header/>
                </div>
                <div>
                    <img src={Loading} alt='Loading...'/>
                </div>
                <div>
                    <Text data={waitMessage}/>
                </div>
            </div>
        </div>
    )
}

export default Wait;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '350px'
};
