import Button from './util/Button'
import Text from './util/Text'
import Input from "./util/Input";
import Header from './util/Header';
import Clock from "./util/Clock";
import { useDispatch } from "react-redux";
import { addAction } from "./ActionQueue";
import { useState } from "react";

const Topic = (props) => {
    const dispatch = useDispatch();
    const [topic1, setTopic1] = useState('');
    const [topic2, setTopic2] = useState('');

    return (
        <div>
            <div style={containerStyle}>
                <div>
                    <Header/>
                </div>
                <div style={playersStyle}>
                    <Text data='Choose topics'/>
                    <Input input={setTopic1} placeholder='First topic'/>
                    <Input input={setTopic2} placeholder='Second topic'/>
                </div>
                <div>
                    <Button onClick={() => {dispatch(addAction({type: "topic", topics: [topic1, topic2]}))}}
                            data='Submit'/>
                    <Clock start='40' />
                </div>
            </div>
        </div>
    )
}

export default Topic;

const containerStyle = {
    height: '700px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '350px'
};

const playersStyle = {
    maxHeight: '400px',
    height: '100%'
}
